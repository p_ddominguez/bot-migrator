import argparse
import sys
import logging


from src.BotMigrator import BotMigrator
from utils.Config import Config

FORMAT = '%(asctime)s %(message)s'
logger = logging.getLogger("BotMigrator")
logging.basicConfig(format=FORMAT, level=logging.INFO)

IMPORT = 'import'
EXPORT = 'export'

"""
HOW TO:
    Edit the config.json for you use case or create your own and adjust the path in the Main.py script.
    Run the export.sh script.
    Grab Coffee...
    Run the import.sh script.
    Grab Coffee...
References:
    In case of future changes to the ARN structure this has been found to be the most reliable resource for valid up
        to date formatting.
    BOTO3 LexV2Model:
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lexv2-models.html
    AWS SAR:
     https://docs.aws.amazon.com/service-authorization/latest/reference/list_amazonlexv2.html#amazonlexv2-resources-for-iam-policies
"""


def run(command, prefix, env) -> None:
    """
    Loads the config.json file from disk and uses it to configure the BotMigrator instance to process the sanitized
    args. :param arg: :return: None
    """
    try:
        config = Config('./config.json', prefix, env)
        logger.debug(f'Config loaded: {config}')
        bot_migrator = BotMigrator(config, logger)
        logger.debug(f'Configuration complete')
        bot_migrator.import_bots() if command == IMPORT else bot_migrator.export_bots()

    except Exception as e:
        logger.error(f'Error during bot migration: {e}')


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('action', help=f'{EXPORT}|{IMPORT} <PREFIX> <ENV>')
    arg_parser.add_argument('prefix', help=f'set the default prefix', type=str)
    arg_parser.add_argument('env', help=f'set the environment', type=str)
    args = arg_parser.parse_args()
    if args.action != IMPORT and args.action != EXPORT:
        logger.error(f'Valid actions include {EXPORT}|{IMPORT}')
        sys.exit(1)
    run(args.action, args.prefix, args.env)
