import logging
import typing


class AliasManager:
    """
    Static methods for importing and exporting properly tagged bot aliases, and associated lambda arns. To export the
    lambdas use the LambdaManager class.
    """

    @staticmethod
    def list_aliases(lex_client, bot_id, next_token=None, alias_summaries=None) -> typing.List:
        """List the aliases associated with this bot"""
        if alias_summaries is None:
            alias_summaries = []
        bot_alias_summaries = alias_summaries
        response = lex_client.list_bot_aliases(botId=bot_id, maxResults=50) if next_token is None else lex_client \
            .list_bot_aliases(botId=bot_id,
                              maxResults=50,
                              nextToken=next_token)
        bot_alias_summaries += response['botAliasSummaries']
        try:
            n_t = response['nextToken']
            if n_t is not None or str(n_t).strip() != '':
                return AliasManager.list_aliases(lex_client, bot_id,
                                                 next_token=n_t,
                                                 alias_summaries=bot_alias_summaries)
            return bot_alias_summaries
        except Exception as e:
            return bot_alias_summaries

    @staticmethod
    def get_lambda_arn(lex_client, bot_alias_id, bot_id, logger: logging.Logger) -> str or None:
        """
        Get the lambda ARN associated with this ARN if any.
        :param lex_client: The boto3 lex client instance reference.
        :param bot_alias_id: The ID of the alias.
        :param bot_id:  The ID of the bot.
        :param logger: logger instance.
        :return: The lambda ARN or None if one does not exist.
        """
        response = lex_client.describe_bot_alias(botAliasId=bot_alias_id, botId=bot_id)
        try:
            l_arn = response['botAliasLocaleSettings']['en_US']['codeHookSpecification']['lambdaCodeHook']['lambdaARN']
            logger.info(f"Associated Lambda ARN: {l_arn}")
            return l_arn
        except Exception as e:
            logger.error(f"Warning: Does this Alias have lambdas? Error: {e}")
        return None
