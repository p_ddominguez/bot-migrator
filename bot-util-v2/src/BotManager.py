import logging
import os
import time
import typing
import boto3

from botocore.exceptions import ClientError

from src.models.BotIntent import BotIntent
from src.models.Export import Export
from utils.Helper import Helper


class BotManager:
    def __init__(self, config, _logger: logging.Logger):
        """
        Base Class contains the necessary methods to interact
        with the lex v2 migration tools
        :param config: Config definition that contains data to instantiate this instance.
        """
        self.logger = _logger
        self.logger.info(f"{self} Loading")
        self.config = config
        self.errors = []
        self.export_path = self.config.bot_export_path
        self.import_path = self.config.bot_export_path
        self.sts_client = boto3.client("sts")
        self.session = boto3.session.Session()
        self.lex_client = boto3.client("lexv2-models")
        self.logs_client = boto3.client("logs")
        self.lambda_client = boto3.client("lambda")
        self.iam_client = boto3.client("iam")
        self.ACCOUNT_ID = self.sts_client.get_caller_identity()["Account"]
        self.REGION = self.session.region_name

    def load_bots(self) -> typing.Dict[str, typing.Any]:
        """
        Load the bots if any from the configured import path and map it back to an Export object to be used for
        processing.
        """
        import_path = self.config.bot_export_path
        if import_path is None:
            raise ValueError(f'Missing import/export path definition!')

        if not os.path.exists(import_path):
            raise ValueError(f'Unable to locate export directory: {import_path}')

        bots = {}
        files = os.listdir(self.config.bot_export_path)
        for file in files:
            bot = Export.from_dict(Helper.load_json(f'{import_path}/{file}'))
            bots.update({bot.name: bot})
        return bots

    def try_create_bot(self, bot_import: Export, role_arn) -> str:
        """
        Try to create a LexV2 bot using the import file. Creates one if it does not already exist.
        :param bot_import: The Export object that was imported.
        :param role_arn: The Amazon Resource Name (ARN) of an IAM role that has permission to access the bot.
        :return: The id of the bot if it was created.
        """
        try:
            bot_def = bot_import.bot_def
            description = bot_def['description']
            response = self.lex_client.create_bot(
                botName=bot_def['name'],
                roleArn=role_arn,
                idleSessionTTLInSeconds=bot_def['idleSessionTTLInSeconds'],
                description='' if description is None else description,
                dataPrivacy=bot_def['dataPrivacy']
            )
            self.logger.info(f'Creat bot response: {response}')
            return response['botId']
        except ClientError as ex:
            error_code = ex.response['Error']['Code']
            error_msg = ex.response['Error']['Message']
            if error_code == 'PreconditionFailedException':
                if f"Bot with name {bot_import.name} already exists." in error_msg:
                    return self.update_bot(bot_import, role_arn)
            self.errors.append(f'Error creating lex bot: {error_msg}')

    # TODO: Migrate to AliasManager. See Intent/ Locale Manager for pattern.
    def try_create_alias(self, bot_import: Export, bot_id, bot_alias) -> str:
        """Attempt to create an alias, if it does not exists, fallbacks to update if it does."""
        try:
            self.logger.info(f"Updating alias: {bot_import.name}::{bot_alias['botAliasName']}")
            version = bot_alias['botVersion']
            desc = bot_alias['description']
            if str(version).lower() != 'draft':
                response = self.lex_client.create_bot_alias(
                    botAliasName=bot_alias['botAliasId'],
                    description='' if desc is None else desc,
                    botVersion=version,
                    botId=bot_id
                )
        except ClientError as ex:
            error_code = ex.response['Error']['Code']
            error_msg = ex.response['Error']['Message']
            if error_code == 'PreconditionFailedException':
                if f"Failed to retrieve parent resource since it does not exist." in error_msg:
                    self.logger.info(f'Version does not exist.')
            self.errors.append(f'Error creating bot alias: {error_msg}')

    def get_bot_alias(self, _name, bot_name):
        try:
            response = self.lex_client.get_bot_alias(
                name=_name,
                botName=bot_name)
            return response['checksum']
        except Exception as e:
            pass

    def get_intent_version(self, intent_name):
        response = self.lex_client.get_intent_versions(
            name=intent_name,
            maxResults=50  # We delete old versions so it shouldn't get this long
        )

        if len(response['intents']) == 0:
            return 0  # We need to raise this, this is just temporary
            # TODO: Whats the point of raising this if you never get here though ? - dan
            # raise Exception(f'Missing required intent {name}')

        latest_version = response['intents'][-1]['version']
        # Validate the accuracy.
        # [0] = $LATEST and [1] is the highest version??
        return latest_version

    def get_bot_version(self, bot_id):
        """
        Gets the latest version of the bot given the bots ID.
        :param bot_id: LexV2 BotID (NOT NAME)
        :return: The latest version of the pub
        """
        bot_versions = self.list_bot_versions(bot_id)
        version_number = bot_versions[-1]['botVersion']
        return version_number

    def get_bot_export_id(self, bot_id, version):
        """
        Creates an export for the given bot version and returns the export Id to be used for polling. 
        The initial state of will be pending.
        :param bot_id: The unique Id of the bot who's export we want.
        :param version: The version of the bot who's export we want.
        :param local_id: The locale id of the bot who's export we want.
        :return: The export id for the bot to be polled.
        """
        resource_spec = {
            'botExportSpecification': {
                'botId': bot_id,
                'botVersion': version
            }
        }

        # This response contains an S3 pre-signed url to retrieve the zip file
        response = self.lex_client.create_export(resourceSpecification=resource_spec,
                                                 fileFormat='LexJson')
        return response['exportId']

    def get_bot_local(self, bot_id, version):
        """
        Obtains the lexv2 locales for the given bot id.
        :param bot_id: The unique id of the bot who's locales we want.
        :param version: The version of the bot who's locales we want.
        :return: The first local id.
        """
        response = self.lex_client.list_bot_locales(
            botId=bot_id,
            botVersion=version,
            maxResults=50
        )
        self.logger.info(f"Bot locales: {response['botLocaleSummaries']}")
        return response['botLocaleSummaries'][0]['localeId']

    def describe_export(self, export_id, retry: int = 3, interval: int = 2) -> str:
        """
        Polls for the download url in the response.
        :param export_id: The id of the export that we want to download.
        :param retry: The number of attempts to make before a timeout is thrown.
        :param interval: The duration to wait between each attempt.
        :return: The url or a TimeOut exception if we don't get the response in the window we've defined.
        """
        url = None
        attempt = 0
        while url is None and attempt != retry:
            attempt += 1
            try:
                response = self.lex_client.describe_export(exportId=export_id)
                url = response['downloadUrl']
            except Exception as e:
                self.logger.error(
                    f'WARNING: {e} not in response retrying in {interval} second(s) attempt {attempt}/{retry}')
                time.sleep(interval)
        return url

    def update_bot(self, bot_import: Export, role_arn):
        """Update an existing bot definition."""
        self.logger.info(f'Updating bot definition...')
        try:
            # filter the list of bots by the given name to get the id.
            bot_id = [bot['botId'] for bot in self.list_bots() if bot['botName'] == bot_import.name]
            if bot_id is None:
                raise ValueError(f'Unable to find {bot_import.name} associated with this account.')
            bot_def = bot_import.bot_def
            description = bot_def['description']
            self.lex_client.update_bot(
                botId=bot_id[0],
                botName=bot_import.name,
                roleArn=role_arn,
                idleSessionTTLInSeconds=bot_def['idleSessionTTLInSeconds'],
                description='' if description is None else description,
                dataPrivacy=bot_def['dataPrivacy']
            )
            return bot_id[0]
        except Exception as e:
            return None

    def list_bot_versions(self, bot_id, next_token=None):
        """
        Obtains a list of all the bot versions for the given bot id
        :param bot_id: The id of the bots versions we want.
        :param next_token: If there are more than 50 versions you will get a next_token
        :return: The list of versions for the bot
        """
        # The new API complains about nextToken being empty so the former approach will no longer work.
        if next_token is None:
            response = self.lex_client.list_bot_versions(
                botId=bot_id,
                maxResults=50  # Default is 10
            )
        else:
            response = self.lex_client.list_bot_versions(
                botId=bot_id,
                maxResults=50,
                nextToken=next_token
            )
        self.logger.debug(f"Bot versions: {response['botVersionSummaries']}")
        bot_versions = response['botVersionSummaries']
        if 'nextToken' in response and response['nextToken'] != '':
            bot_versions = bot_versions + self.list_bot_versions(bot_id, next_token=response['nextToken'])
        return bot_versions

    def list_bots(self, next_token=''):
        """
        Obtain a list of bots from the configured account and env.
        :param next_token: If there are more results this token is used.
        :return: The list of bot summaries
        """
        response = self.lex_client.list_bots()
        bots = response['botSummaries']
        self.logger.debug(f"Bot Summaries: {response['botSummaries']}")
        if 'nextToken' in response and response['nextToken'] != '':
            bots = bots + self.list_bots(next_token=response['nextToken'])
        return bots
