import os
import json

from src.AliasManager import AliasManager
from src.BotManager import BotManager
from src.IntentManager import IntentManager
from src.LambdaManager import LambdaManager
from src.LocaleManager import LocaleManager
from src.SlotManager import SlotManager
from src.models.BotAlias import BotAlias, AwsBotAliasDef
from src.models.BotIntent import BotIntent
from src.models.BotLocale import BotLocale
from src.models.Export import Export
from utils.Config import Config
from utils.Helper import Helper


class BotMigrator(BotManager):
    def __init__(self, config: Config, logger):
        """
        The Bot Migrator is a utility for automating the migration of LexV2 bots from one env to another.
        :param config: Config files can be define via json or instantiate in code.
        """
        super().__init__(config, logger)

    def import_bots(self) -> None:
        """
        Loads exported bot definitions from disk and provisions them.
        :return: None
        """
        imported_bots = self.load_bots()
        self.logger.info(f'Imported bot(s): {", ".join(imported_bots.keys())}')

        bot: Export
        for bot_name, bot in imported_bots.items():
            self.logger.info(f'Updating bot {bot_name}')
            lex_role_arn = Helper.ArnBuilder.lexv2_role(self.ACCOUNT_ID, self.config.lex_role)
            bot_id = self.try_create_bot(bot, lex_role_arn)
            if bot_id is None:
                self.logger.error(f'Failed to update bot: {bot_name}')
                self.errors.append(f'Failed to update bot: {bot_name}')
            else:
                # create the bot locales
                LocaleManager.update_locales(self.logger, self.lex_client, bot_id, bot)

                # create the bot intents
                IntentManager.update_intents(self.logger, self.lex_client, bot_id, bot)

                # create bot version TODO: Add this flow.

                # create the aliases TODO: Finish version flow before uncommenting / Migrate to AliasManager
                # alias_ids = [self.try_create_alias(bot, bot_id, alias) for alias in bot.aliases]

        self.logger.error(f'Completed import with {len(self.errors)} error(s).')
        [self.logger.error(f'WARNING: {er}') for er in self.errors]

    def export_bots(self) -> None:
        """
        Retrieves bot data from the given account and region and exports them to disk in json format.
        :return: None
        """
        if not os.path.exists(self.config.bot_export_path):
            os.makedirs(self.config.bot_export_path)

        bots = self.list_bots()
        for bot in bots:
            bot_name, bot_id = bot['botName'], bot['botId']
            # API does not return ARN :(
            bot_arn = Helper.ArnBuilder.lexv2_arn(self.REGION, self.ACCOUNT_ID, bot_id)
            if Helper.is_export(self.lex_client, bot_arn, self.config.export_key) is not True:
                self.logger.info(f"WARNING: {bot_name} - Not tagged for export, skipping!")
                continue

            self.logger.info(f"Preparing: {bot_name} for export.")
            version = self.get_bot_version(bot_id)
            export_id = self.get_bot_export_id(bot_id, version)
            aliases = AliasManager.list_aliases(self.lex_client, bot_id)
            self.logger.debug(f'Bot Aliases: {aliases}')

            # Create the export object.
            export = Export(bot_name, bot_id, bot_arn, version, export_id)
            for alias in aliases:
                alias = BotAlias.from_dict(alias)  # Map the structure to an object
                alias.alias_arn = Helper.ArnBuilder.lexv2_alias_arn(self.REGION, self.ACCOUNT_ID, bot_id,
                                                                    alias.botAliasId)
                if Helper.is_export(self.lex_client, alias.alias_arn, self.config.export_key):
                    alias.lambda_arn = AliasManager.get_lambda_arn(self.lex_client, alias.botAliasId, bot_id,
                                                                   self.logger)

                    # TODO: Verify we don't need this when migrating.
                    if alias.lambda_arn is not None:
                        alias.lambda_download_url = LambdaManager.get_download_url(self.lambda_client,
                                                                                   alias.lambda_arn,
                                                                                   self.logger)

                    """Polling for the download URL to be in the response from AWS."""
                    url = self.describe_export(export_id, retry=self.config.retry_attempts,
                                               interval=self.config.attempt_interval)
                    if url is None:
                        raise TimeoutError("Ran out of time waiting for AWS to provision the resource...")

                    export.export_url = url
                    export.aliases.append(alias)
                    export.fetch_bot_export()
                    export.write_to_disk(self.config.bot_export_path)
                    # bot_config = Helper.download_bot_export(url, self.config.bot_export_path, bot_name)
                    self.logger.info(f"Exporting: {bot_name} completed!")
                else:
                    self.logger.info(f"WARNING: {alias.botAliasName} - Not tagged for export, skipping!")
