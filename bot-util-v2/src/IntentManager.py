import logging

from botocore.exceptions import ClientError

from src.models.BotIntent import BotIntent
from src.models.BotLocale import BotLocale
from src.models.Export import Export
from utils.Helper import Helper


class IntentManager:
    """Static helper class that encapsulates methods associated with intent management."""
    @staticmethod
    def try_create_intent(logger: logging.Logger, lex_client, bot_id, bot_import: Export, locale_id, intent: BotIntent):
        if intent.name == 'FallbackIntent':
            return

        logger.info(f"Updating intent: {bot_import.name}::{intent.name}")
        try:
            response = lex_client.create_intent(**Helper.RequestBuilder.create_intent_request(intent, bot_id, locale_id))

        except ClientError as ex:
            error_code = ex.response['Error']['Code']
            error_msg = ex.response['Error']['Message']
            if error_code == 'ValidationException':
                if f"Intent with name {intent.name} already exists." in error_msg:
                    IntentManager.update_intent(logger, lex_client, bot_id, bot_import, locale_id, intent)

    @staticmethod
    def update_intent(logger: logging.Logger, lex_client, bot_id, bot_import: Export, locale_id, intent: BotIntent):
        # Get a list of the intents for this bot  with the current locale, and filter down to the intent with a matching
        # name. Since the identifier will be different it is important to compare the name.
        intent_ids = [x['intentId'] for x in IntentManager.list_intents(lex_client, bot_id, locale_id) if
                      x['intentName'] == intent.name]
        if len(intent_ids) > 0:
            request_body = Helper.RequestBuilder.create_intent_request(intent, bot_id, locale_id)
            request_body.update({'intentId': intent_ids[0]})
            response = lex_client.update_intent(**request_body)

    @staticmethod
    def list_intents(lex_client, bot_id, locale_id, version='DRAFT'):
        """List the intents for the given bot and locale, default sto the DRAFT version if none provided."""
        response = lex_client.list_intents(
            botId=bot_id,
            botVersion=version,
            localeId=locale_id
        )
        return response['intentSummaries']

    @staticmethod
    def update_intents(logger, lex_client, bot_id, bot):
        """Update the intents for the given bot. Attempts to create them first, and falls back to update if that 
        fails. """
        for locale in bot.locales.values():
            locale = BotLocale.from_dict(locale)
            for intent in locale.intents.values():
                IntentManager.try_create_intent(logger, lex_client, bot_id, bot,
                                                locale.identifier, BotIntent.from_dict(intent))
