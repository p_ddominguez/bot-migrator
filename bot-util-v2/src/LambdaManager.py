import logging


class LambdaManager:
    @staticmethod
    def get_download_url(lambda_client, lambda_arn, logger: logging.Logger) -> str:
        """
        Fetch the download link associated with URL. This link is only valid for 10 min.
        :param lambda_client: The Boto3 client to use when making this request.
        :param lambda_arn: The arn of the lambda we want to fetch
        :param logger: Logger instance.
        :return: A url to the lambda or throws an exception.
        """
        response = lambda_client.get_function(
            FunctionName=lambda_arn
        )
        try:
            url = response['Code']['Location']
            # logger.info(f"Lambda URL: {url}")
            return url
        except Exception as e:
            raise Exception(f"Error trying to retrieve lambda url! {e}")
