import typing


class AwsBotAliasDef:
    """A Bot Alias definition according to AWS documentation. Use this class when your intention is to map to or from
    a valid aws bot alias. IE your import should not use the raw BotAlias as it contains domain specific data that 
    the aws API will not understand. Notice the use of camel vs snake case!"""

    def __init__(self):
        self.botAliasId: str = None
        self.botAliasName: str = None
        self.description: str = None
        self.botVersion: str = None
        self.botAliasStatus: str = None
        self.creationDateTime: typing.Dict = None
        self.lastUpdatedDateTime: typing.Dict = None

    @staticmethod
    def from_model(alias_model):
        """Striped domain data from the object and matches the current aws definition"""
        aws_bot_alias = AwsBotAliasDef()
        for k, v in alias_model.__dict__.items():
            if k in aws_bot_alias.__dict__.keys():
                aws_bot_alias.__dict__[k] = v
        return aws_bot_alias


class BotAlias(AwsBotAliasDef):
    """ This class extends the AWS definition of a bot alias so we can append domain specific properties to the object.
    Each implementation of the model contains a mapping method to convert to and from the DTO and the API Defined mode."""

    def __init__(self):
        # domain specific
        super().__init__()
        self.alias_arn: str = None
        self.lambda_arn: str = None
        self.lambda_download_url: str = None

    @staticmethod
    def from_dict(alias_dict):
        """Takes a standard python dict object and maps it to and object to perform operations on."""
        aws_bot_alias = BotAlias()
        for k, v in alias_dict.items():
            if k in aws_bot_alias.__dict__.keys():
                aws_bot_alias.__dict__[k] = v
        return aws_bot_alias

    @staticmethod
    def from_aws_def(aws_bot_alias_obj):
        """Takes in an AwsBotAliasDef object and maps it to a DTO"""
        bot_alias_dto = BotAlias()
        for k, v in aws_bot_alias_obj.__dict__.items():
            if k in bot_alias_dto.__dict__.keys():
                bot_alias_dto.__dict__[k] = v
        return bot_alias_dto
