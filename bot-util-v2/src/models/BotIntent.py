import typing


class AwsBotIntentDef:
    def __init__(self):
        self.name: str = None
        self.identifier: str = None
        self.description: str = None
        self.parentIntentSignature: str = None
        self.sampleUtterances: typing.List = []
        self.intentConfirmationSetting: typing.Dict = {}
        self.intentClosingSetting: typing.Dict = {}
        self.inputContexts: typing.List = []
        self.outputContexts: typing.List = []
        self.kendraConfiguration: typing.Dict = {}
        self.dialogCodeHook: typing.Dict = {}
        self.fulfillmentCodeHook: typing.Dict = {}
        self.slotPriorities: typing.List = []

    @staticmethod
    def from_model(intent_model):
        """Striped domain data from the object and matches the current aws definition"""
        aws_bot_locale = AwsBotIntentDef()
        for k, v in intent_model.__dict__.items():
            if k in aws_bot_locale.__dict__.keys():
                aws_bot_locale.__dict__[k] = v
        return aws_bot_locale


class BotIntent(AwsBotIntentDef):
    """ This class extends the AWS definition of a bot intent so we can append domain specific properties to the object.
Each implementation of the model contains a mapping method to convert to and from the DTO and the API Defined mode."""
    def __init__(self):
        super().__init__()
        self.slots: typing.Dict = {}

    @staticmethod
    def from_dict(locale_dict):
        """Takes a standard python dict object and maps it to and object to perform operations on."""
        aws_bot_locale = BotIntent()
        for k, v in locale_dict.items():
            if k in aws_bot_locale.__dict__.keys():
                aws_bot_locale.__dict__[k] = v
        return aws_bot_locale

    @staticmethod
    def from_aws_def(aws_bot_locale_obj):
        """Takes in an AwsBotIntentDef object and maps it to a DTO"""
        bot_locale_dto = BotIntent()
        for k, v in aws_bot_locale_obj.__dict__.items():
            if k in bot_locale_dto.__dict__.keys():
                bot_locale_dto.__dict__[k] = v
        return bot_locale_dto
