import random
import unittest
import zipfile

from src.models.Export import Export
from src.tests.TestUtils import TestUtils


class ExportTests(unittest.TestCase):

    def test_extract_data(self):
        # Given: A bot export with data to extract.
        given_export_path = 'src/tests/resources/test-bot-export.zip'
        given_export = TestUtils.load_zip(given_export_path)
        self.assertIsNotNone(given_export, msg="Unable to load mock export!")

        # Given: An Actual
        actual = Export(
            bot_name="foo",
            bot_id="foo-bar",
            bot_arn="foo-arn-bar",
            bot_version='FOO',
            bot_export_id='FOOBAR')

        # When: extracting the data from the bot export.
        with zipfile.ZipFile(given_export_path) as zip_data:
            zip_list = zip_data.infolist()
            actual.prioritized_extract_data(zip_data, zip_list)

        # Then: Assert that the export object contains the expected data
        self.assertEqual(len(actual.locales), 1)
        self.assertEqual(len(actual.aliases), 0)
        self.assertEqual(len(actual.locales['en_US'].intents), 2)
        self.assertEqual(len(actual.locales['en_US'].slot_types), 1)
        self.assertEqual(len(actual.locales['en_US'].slot_types['TestSlotType']['slotTypeValues']), 6)

    def test_extract_data_random_order(self):
        """Test the ability to load and properly parse a bot export regardless of the order of the files inside."""
        # Given: A bot export with data to extract.
        given_export_path = 'src/tests/resources/test-bot-export.zip'
        given_export = TestUtils.load_zip(given_export_path)
        self.assertIsNotNone(given_export, msg="Unable to load mock export!")

        # Given: An Actual
        actual = Export(
            bot_name="foo",
            bot_id="foo-bar",
            bot_arn="foo-arn-bar",
            bot_version='FOO',
            bot_export_id='FOOBAR')

        # When: extracting the data from the bot export.
        with zipfile.ZipFile(given_export_path) as zip_data:
            zip_list = zip_data.infolist()
            random.shuffle(zip_list)
            actual.prioritized_extract_data(zip_data, zip_list)

        # Then: Assert that the export object contains the expected data
        self.assertEqual(len(actual.locales), 1)
        self.assertEqual(len(actual.aliases), 0)
        self.assertEqual(len(actual.locales['en_US'].intents), 2)
        self.assertEqual(len(actual.locales['en_US'].slot_types), 1)
        self.assertEqual(len(actual.locales['en_US'].slot_types['TestSlotType']['slotTypeValues']), 6)
