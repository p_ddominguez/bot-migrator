import json
import os


class AbstractConfig:
    def __init__(self):
        """
        Dynamic Config class definition. Loads a json object from a file and maps its values to itself. Resolves the
        values using standard expression language resolution starting with env variables.
        """
        self.retry_attempts = 3
        self.attempt_interval = 2
        self.default_prefix = None
        self.environment = None
        self.bot_export_path = None
        self.big_slot_count = None
        self.truncate_big_slot = None
        self.bot_alias = None
        self.export_key = None
        self.log_group = None
        self.log_role = None
        self.config_json = None
        self.lex_role = None

    def set_values(self):
        for k, v in self.config_json.items():
            if k in self.__dict__.keys():
                self.__dict__[k] = v


class Config(AbstractConfig):
    def __init__(self, config_path, prefix, env):
        super().__init__()
        self.config_path = config_path
        self.load_config(prefix, env)
        self.set_values()

    def __repr__(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    def load_config(self, prefix, env) -> None:
        """
        Dynamically loads a JSON configuration and maps itself to an object.
        Environment Variables start with $ and are enclosed in {} brackets.
        Configuration Reference Variables start with # and are enclosed in {} brackets
        eg. Sharing config values
        Configuration Reference expressions are resolved post Environment Variables in-case they are referenced
        later in the config
        :return: None
        """
        try:
            with open(self.config_path, 'r') as config_file:
                self.config_json = json.load(config_file)
                self.config_json['default_prefix'] = prefix
                self.config_json['environment'] = env
                [self.compile_env_expressions(k, v) for k, v in self.config_json.items()]
                [self.compile_config_expressions(k, v) for k, v in self.config_json.items()]
        except Exception as e:
            print(f'Error loading config: {e}')

    def compile_env_expressions(self, key, value) -> None:
        """
        Compiles an expression if present that will resolve to the given value,
        :param key: The objects key
        :param value: The current value being processed.
        :return: None
        """
        if '$' in str(value):
            args = value.strip().split('$')
            self.config_json[key] = ""
            for arg in args:
                if arg is not None:
                    if len(arg.strip()) != 0:
                        if arg.startswith('{'):
                            if str(arg).endswith('}'):
                                self.config_json[key] += os.environ.get(str(arg).replace('{', "").replace('}', ""))
                            else:
                                caboose = str(arg).split('}')
                                isolated_variable = str(caboose[0]).replace('{', "").replace('}', "")
                                self.config_json[key] += str(os.environ.get(str(isolated_variable)
                                                                            .replace('{', "")
                                                                            .replace('}', ""))) + caboose[1]
                        else:
                            self.config_json[key] += arg

    def compile_config_expressions(self, key, value) -> any:
        """
        Compiles an expression if present that will resolve to the given value,
        :param key: The objects key
        :param value: The current value being processed.
        :return: None
        """
        if '#' in str(value):
            args = value.strip().split('#')
            self.config_json[key] = ""
            for arg in args:
                if arg is not None:
                    if len(arg.strip()) != 0:
                        if arg.startswith('{'):
                            if str(arg).endswith('}'):
                                self.config_json[key] += self.config_json[str(arg).replace('{', "").replace('}', "")]
                            else:
                                caboose = str(arg).split('}')
                                isolated_variable = str(caboose[0]).replace('{', "").replace('}', "")
                                self.config_json[key] += self.config_json[isolated_variable] + caboose[1]
                        else:
                            self.config_json[key] += arg
