import io
import json
import os
import urllib
import zipfile
from typing import Dict, Any

from src.models.BotIntent import BotIntent


class ArnBuilder:
    @staticmethod
    def lexv2_arn(region, account_id, bot_id):
        return f"arn:aws:lex:{region}:{account_id}:bot/{bot_id}"

    @staticmethod
    def lexv2_alias_arn(region, account_id, bot_id, alias_id):
        return f"arn:aws:lex:{region}:{account_id}:bot-alias/{bot_id}/{alias_id}"

    @staticmethod
    def lexv2_role(account_id, role_name):
        return f"arn:aws:iam::{account_id}:role/{role_name}"


class RequestBuilder:
    @staticmethod
    def create_intent_request(intent: BotIntent, bot_id, locale_id, _version="DRAFT") -> dict:
        """Takes in the bots intent export object and stores the non-null values in a dict to be passed
        as args to the create_intent method. Some bots may or may not contain these properties, and allowing None to be
        passed in will cause errors. This method only passes the arg if the value is not None."""
        request_body = {}
        request_body.update({'intentName': intent.name})
        if intent.description is not None:
            request_body.update({'description': intent.description})
        if intent.parentIntentSignature is not None:
            request_body.update({'parentIntentSignature': intent.parentIntentSignature})
        if intent.sampleUtterances is not None:
            request_body.update({'sampleUtterances': intent.sampleUtterances})
        if intent.dialogCodeHook is not None:
            request_body.update({'dialogCodeHook': intent.dialogCodeHook})
        if intent.fulfillmentCodeHook is not None:
            code_hook_body = {}
            for k, v in intent.fulfillmentCodeHook.items():
                if v is not None:
                    code_hook_body.update({k: v})
            request_body.update({'fulfillmentCodeHook': code_hook_body})
        if intent.intentConfirmationSetting is not None:
            confirmation_body = {}
            confirmation_body.update({'active': True})

            # TODO: Extract method
            prompt_spec = {}
            retries = intent.intentConfirmationSetting['promptSpecification']['maxRetries']
            message_groups = intent.intentConfirmationSetting['promptSpecification']['messageGroupsList']
            msg_groups = Helper.scrub_message_groups(message_groups)
            prompt_spec.update({'messageGroups': msg_groups})
            prompt_spec.update({'maxRetries': retries})
            prompt_spec.update({'allowInterrupt': True})
            confirmation_body.update({'promptSpecification': prompt_spec})

            # TODO: Extract method
            decline_response = {}
            message_groups = intent.intentConfirmationSetting['declinationResponse']['messageGroupsList']
            msg_groups = Helper.scrub_message_groups(message_groups)
            decline_response.update({'messageGroups': msg_groups})
            decline_response.update({'allowInterrupt': True})
            confirmation_body.update({'declinationResponse': decline_response})

            request_body.update({'intentConfirmationSetting': confirmation_body})
        if intent.intentClosingSetting is not None:
            request_body.update({'intentClosingSetting': intent.intentClosingSetting})
        if intent.inputContexts is not None:
            request_body.update({'inputContexts': intent.inputContexts})
        if intent.outputContexts is not None:
            request_body.update({'outputContexts': intent.outputContexts})
        if intent.kendraConfiguration is not None:
            request_body.update({'kendraConfiguration': intent.kendraConfiguration})
        request_body.update({'botId': bot_id})
        request_body.update({'botVersion': 'DRAFT'})
        request_body.update({'localeId': locale_id})
        return request_body


class Helper:
    """
    Helper class for shared or orphaned methods
    """
    ArnBuilder = ArnBuilder
    RequestBuilder = RequestBuilder

    @staticmethod
    def log_group_exists(logs_client, log_group_name):
        """
        Helper method to check if a given log group exists.
        :param logs_client: The log boto3 client for the helper to use.
        :param log_group_name: The name of the log group you want to check.
        :return: True if the log group exists, else false
        """
        response = logs_client.describe_log_groups(
            logGroupNamePrefix=log_group_name,
            limit=10
        )
        for log_group in response['logGroups']:
            if log_group['logGroupName'] == log_group_name:
                return True
        return False

    @staticmethod
    def role_exists(iam_client, role_name):
        """
        Helper method to check if a given role exists.
        :param iam_client: The iam boto3 client for the helper to use.
        :param role_name: The name of the role you want to check.
        :return: True if the role exists, else false
        """
        try:
            response = iam_client.get_role(
                RoleName=role_name
            )
        except iam_client.exceptions.NoSuchEntityException:
            return False
        return True

    @staticmethod
    def is_export(lex_client, resource_arn, export_key):
        """
        Gets a list of the tags associated with the resource and exports if the export_key is found and its value is true.
        :param lex_client: The client to be used to handle the request
        :param resource_arn: The complete arn of the resource to check
        :param export_key: The key to check against the resources tags
        :return: True if it is tagged, false if it is not.
        """
        response = lex_client.list_tags_for_resource(resourceARN=resource_arn)
        try:
            for t in response['tags']:
                if str(t).lower() == export_key.lower() and str(response['tags'][t]).lower() == 'true':
                    return True
        except Exception as ex:
            print(f'Error when processing tags: {ex}')

    @staticmethod
    def download_lambda_export(url, out_dir, bot_name, lambda_name):
        base_path = f'{out_dir}/{bot_name}/lambdas/{lambda_name}'
        if not os.path.exists(base_path):
            os.makedirs(base_path)

        with urllib.request.urlopen(url) as dl_file:
            with open(f'{base_path}/{lambda_name}.zip', 'wb') as out_file:
                out_file.write(dl_file.read())

    @staticmethod
    def download_bot_export(url, out_dir, bot_name):
        base_path = f'{out_dir}/{bot_name}'
        if not os.path.exists(base_path):
            os.makedirs(base_path)

        with urllib.request.urlopen(url) as r_zip:
            with zipfile.ZipFile(io.BytesIO(r_zip.read())) as zip_data:
                zip_list = zip_data.infolist()
                for z_file in zip_list:
                    flat_name = "::".join(z_file.filename.split('/'))
                    compiled_path = f'{base_path}/{flat_name}'
                    with open(compiled_path, 'wt') as out_file:
                        with zip_data.open(z_file) as curr_file:
                            out_file.write(json.dumps(json.loads(curr_file.read().decode('utf-8')), indent=4))

    @staticmethod
    def load_json(path):
        try:
            with open(path) as json_data:
                return json.loads(json_data.read())
        except Exception as e:
            return None

    @staticmethod
    def scrub_message_groups(message_groups: list):
        _groups = []
        for group in message_groups:
            new_group = {}
            for key, val in group.items():
                if val is not None:
                    if isinstance(val, dict):
                        new_dict = {}
                        for k, v in val.items():
                            if v is not None:
                                new_dict.update({k: v})
                        new_group.update({key: new_dict})
            _groups.append(new_group)
        return _groups
